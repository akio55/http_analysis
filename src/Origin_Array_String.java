import java.io.*;
import java.util.ArrayList;

/**
 * Created by akio on 2017/05/09.
 */
public class Origin_Array_String {
    private static int N = 10000;

    private ArrayList<String> cache = new ArrayList<>();
    private int Arraycnt = 0;
    private int PageNumber = 0;
    private int PageNumber_max = 0;
    private String FileName;

    public void Start(String Name) {
        FileName= Name;
    }

    public void add(String str) {
        if(Arraycnt / N == PageNumber) {
            cache.add(str);
            Arraycnt++;
        } else {

            File file_write = new File(FileName + PageNumber + ".dat");
            File file_read = new File(FileName + PageNumber_max + ".dat");


            if(PageNumber == PageNumber_max) {
                PageNext(file_write);
                PageNumber_max++;
            } else {
                PageChange(file_write, file_read);
            }

            cache.add(str);
            PageNumber = PageNumber_max;
            Arraycnt++;
        }
    }

    public void set(int cnt, String str) {
        if(cnt/N == PageNumber) {
            cache.set(cnt, str);
        } else {
            File file_write = new File(FileName + PageNumber + ".dat");
            File file_read = new File(FileName + cnt/N + ".dat");

            PageChange(file_write, file_read);

            cache.add(str);
            PageNumber = cnt/N;
            Arraycnt++;
        }
    }

    public String get(int cnt) {
        if(cnt/N == PageNumber) {
            return cache.get(cnt%N);
        } else {
            File file_write = new File(FileName + PageNumber + ".dat");
            File file_read = new File(FileName + cnt/N + ".dat");

            PageChange(file_write, file_read);

            PageNumber = cnt/N;

            return cache.get(cnt%N);
        }
    }

    public Integer size() {
        return Arraycnt;
    }

    private void PageChange(File file_write, File file_read) {
        int i;
        String line;

        try {

            FileWriter fw = new FileWriter(file_write);
            BufferedWriter bw = new BufferedWriter(fw);
            FileReader fr = new FileReader(file_read);
            BufferedReader br = new BufferedReader(fr);

            for(i = 0; i < cache.size(); i++) {
                bw.write(cache.get(i));
                bw.newLine();
            }

            bw.close();
            fw.close();

            cache = new ArrayList<>();

            while((line = br.readLine()) != null) {
                cache.add(line);
            }

            br.close();
            fr.close();


        } catch (FileNotFoundException e) {
            System.out.println(e.getMessage());
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }

    private void  PageNext(File file_write) {
        int i;
        try {
            FileWriter fw = new FileWriter(file_write);
            BufferedWriter bw = new BufferedWriter(fw);

            for(i = 0; i < cache.size(); i++) {
                bw.write(cache.get(i));
                bw.newLine();
            }

            bw.close();
            fw.close();

            cache = new ArrayList<>();

        } catch (FileNotFoundException e) {
            System.out.println(e.getMessage());
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }
}
