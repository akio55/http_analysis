import com.sun.org.apache.xpath.internal.operations.Bool;
//import com.sun.org.apache.xpath.internal.operations.String;
import javafx.embed.swing.JFXPanel;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.event.ActionEvent;
import java.io.File;
import java.io.FileNotFoundException;
import java.rmi.activation.ActivationID;
import java.util.ArrayList;

/**
 * Created by akio on 2017/05/07.
 */
public class Controller {
    private View V = new View();
    private Input_Data InDa = new Input_Data();
    private FileInput_Model FIM = new FileInput_Model();
    private Analysis_Model AM = new Analysis_Model();

    private JFrame frame;
    private JPanel panel;
    private File[] FileName;
    private Boolean LenFlag = false;
    private String LenStart;
    private String LenEnd;

    public void Start() {
        InDa.Start();
        Frame_Settings();
        bind_FileSelect();
        bind_LenSet();
    }


    private void Frame_Settings() {
        frame = new JFrame("アクセス集計");
        panel = V.panel();
        frame.add(panel);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(1000, 720);
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
    }

    private void Start_Analysis() {
        int i;
        DefaultTableModel DTMT = V.get_TableModel_Time();
        DefaultTableModel DTMH = V.get_TableModel_Host();
        DefaultTableModel DTMR = V.get_TableModel_Rank();

        FIM.Input(InDa, FileName, LenFlag, LenStart, LenEnd);
        AM.Start(InDa);

        //String[][] tmp = InDa.get_AnalysisTime_Data();
        String[] tmp = new String[2];
        for(i = 0; i < InDa.AT_DataTime().size(); i++) {

            tmp[0] = InDa.AT_DataTime().get(i);
            tmp[1] = InDa.AT_Datacnt().get(i);
            DTMT.addRow(tmp);
        }

        //String[][] tmp;
        for(i = 0; i < InDa.AH_DataName().size(); i++) {
            tmp[0] = InDa.AH_DataName().get(i);
            tmp[1] = InDa.AH_Datacnt().get(i);
            DTMH.addRow(tmp);
        }

        //String[][] tmp1 = AM.getAnalysisRank_Data();
        for(i = 0; i < InDa.AR_DataName().size(); i++) {
            tmp[0] = InDa.AR_DataName().get(i);
            tmp[1] = InDa.AR_Datacnt().get(i);
            DTMR.addRow(tmp);
        }



    }

    private Action Action_FileSelect = new AbstractAction() {
        @Override
        public void actionPerformed(ActionEvent e) {
           JFileChooser filechose = new JFileChooser();
           filechose.setMultiSelectionEnabled(true);

           int select = filechose.showOpenDialog(frame);

           if(select == JFileChooser.APPROVE_OPTION) {
               FileName = filechose.getSelectedFiles();
           }

           Start_Analysis();
        }
    };

    private Action Action_LenSet = new AbstractAction() {
        @Override
        public void actionPerformed(ActionEvent e) {
            LenFlag = true;
            LenStart = V.get_LenStart().getText();
            LenEnd = V.get_LenEnd().getText();
        }
    };


    private void bind_FileSelect() {
        JButton get_button = V.get_FileSelect();

        get_button.setAction(Action_FileSelect);
        get_button.setText("ファイル選択");
    }

    private void bind_LenSet() {
        JButton get_button = V.get_LenSet();

        get_button.setAction(Action_LenSet);
        get_button.setText("期間設定");
    }
}
