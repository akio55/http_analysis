import com.sun.org.apache.xpath.internal.operations.Bool;
import org.omg.CORBA.INTERNAL;

import java.io.*;
import java.util.ArrayList;

/**
 * Created by akio on 2017/05/07.
 */
public class Input_Data {
    private static int N = 10;
    public Origin_Array_String HostName = new Origin_Array_String();
    public Origin_Array_Integer TimeDay = new Origin_Array_Integer();
    public Origin_Array_String TimeMonth = new Origin_Array_String();
    public Origin_Array_Integer TimeYear = new Origin_Array_Integer();
    public Origin_Array_Integer TimeHour = new Origin_Array_Integer();
    public Origin_Array_String AT_DataTime = new Origin_Array_String();
    public Origin_Array_String AT_Datacnt = new Origin_Array_String();
    public Origin_Array_String AH_DataName = new Origin_Array_String();
    public Origin_Array_String AH_Datacnt = new Origin_Array_String();
    public Origin_Array_String AR_DataName = new Origin_Array_String();
    public Origin_Array_String AR_Datacnt = new Origin_Array_String();
    public Origin_Array_String ATTime = new Origin_Array_String();
    public Origin_Array_Integer ATcnt = new Origin_Array_Integer();


    public void Start() {
        HostName.Start("HostName");
        TimeDay.Start("TimeDay");
        TimeMonth.Start("TimeMonth");
        TimeYear.Start("TimeYear");
        TimeHour.Start("TimeHour");
        AT_DataTime.Start("AT_DataTime");
        AT_Datacnt.Start("AT_Datacnt");
        AH_DataName.Start("AH_DataName");
        AH_Datacnt.Start("AH_Datacnt");
        AR_DataName.Start("AR_DataName");
        AR_Datacnt.Start("AR_Datacnt");
        ATTime.Start("ATTime");
        ATcnt.Start("ATcnt");
    }


    public Origin_Array_String HostName() {
        return HostName;
    }

    public Origin_Array_Integer TimeDay() {
        return TimeDay;
    }

    public Origin_Array_String TimeMonth() {
        return TimeMonth;
    }

    public Origin_Array_Integer TimeYear() {
        return TimeYear;
    }

    public Origin_Array_Integer TimeHour() {
        return TimeHour;
    }

    public Origin_Array_String AT_DataTime() {
        return AT_DataTime;
    }

    public Origin_Array_String AT_Datacnt() {
        return AT_Datacnt;
    }

    public Origin_Array_String AH_DataName() {
        return AH_DataName;
    }

    public Origin_Array_String AH_Datacnt() {
        return AH_Datacnt;
    }

    public Origin_Array_String AR_DataName() {
        return AR_DataName;
    }

    public Origin_Array_String AR_Datacnt() {
        return AR_Datacnt;
    }

    public Origin_Array_String ATTime() {
        return ATTime;
    }

    public Origin_Array_Integer ATcnt() {
        return ATcnt;
    }

}
