import com.sun.org.apache.xpath.internal.operations.Bool;

import javax.swing.table.DefaultTableModel;
import java.util.ArrayList;

/**
 * Created by akio on 2017/05/07.
 */
public class Analysis_Model {
    private Input_Data InDa;

    public void Start(Input_Data InDa_tmp) {
        InDa = InDa_tmp;
        Analysis_time();
        Analysis_HostName();
    }

    private void Analysis_time() {
        int i,j;
        Boolean flag = false;
        //ArrayList<String> Time = new ArrayList<>();
        //ArrayList<Integer> cnt = new ArrayList<>();
        //Time.add("Dummy");
        //cnt.add(0);

        //System.out.println("size:" + InDa.get_Size());
        for(i = 0; i < InDa.HostName().size(); i++) {
            System.out.println(i);
            String str = InDa.TimeYear().get(i) + "/" + InDa.TimeMonth().get(i) + "/" + InDa.TimeDay().get(i) + ":" + InDa.TimeHour().get(i);

            if(InDa.ATTime().size() == 0) {
                InDa.ATTime().add(str);
                InDa.ATcnt().add(1);
            } else {
                for(j = 0; j < InDa.ATTime().size(); j++) {
                    System.out.println(j);
                    if(InDa.ATTime().get(j).equals(str)) {
                        InDa.ATcnt().set(j, InDa.ATcnt().get(j) + 1);
                        flag = true;
                        break;
                    }
                }

                if(!flag) {
                    InDa.ATTime().add(str);
                    InDa.ATcnt().add(1);
                } else {
                    flag = false;
                }
            }
        }

        //InDa.AnalysisTime_Data = new String[cnt.size()][2];

        for(i = 0; i < InDa.ATTime().size(); i++) {
            //System.out.println(Time.get(i) + "  " + cnt.get(i));
            InDa.AT_DataTime().add(InDa.ATTime().get(i));
            InDa.AT_Datacnt().add(String.valueOf(InDa.ATcnt().get(i)));
        }




    }

    private void Analysis_HostName() {
        int i,j;
        int tmp;
        String tmp_str;
        Boolean flag = false;
        ArrayList<String> HostName = new ArrayList<>();
        ArrayList<Integer> cnt = new ArrayList<>();
        ArrayList<String> HostRank = new ArrayList<>();
        ArrayList<Integer>cntRank = new ArrayList<>();

        for(i = 0; i < InDa.HostName().size(); i++) {
            String str = InDa.HostName().get(i);
            System.out.println(str);
            if(HostName.size() == 0) {
                HostName.add(str);
                cnt.add(1);
            } else {
                for(j = 0; j < HostName.size(); j++) {
                    if(HostName.get(j).equals(str)) {
                        cnt.set(j, cnt.get(j) + 1);
                        flag = true;
                        break;
                    }
                }

                if(!flag) {
                    HostName.add(str);
                    cnt.add(1);

                } else {
                    flag = false;
                }
            }
        }

        //AnalysisHost_Data = new String[cnt.size()][2];

        for(i = 0; i < cnt.size(); i++) {
            //System.out.println(HostName.get(i) + "  " + cnt.get(i));
            InDa.AH_DataName().add(HostName.get(i));
            InDa.AH_Datacnt().add(String.valueOf(cnt.get(i)));
        }

        //Bubble Sort

        //System.out.println("アクセスが多い順");

        cntRank = cnt;
        HostRank = HostName;
        for(i = 0; i < cntRank.size(); i++) {
            for(j = cntRank.size() - 1; j > i; j--) {
                if(cntRank.get(j) > cntRank.get(j - 1)) {
                    tmp = cntRank.get(j);
                    tmp_str = HostRank.get(j);
                    cntRank.set(j, cntRank.get(j - 1));
                    HostName.set(j, HostName.get(j - 1));
                    cntRank.set(j - 1, tmp);
                    HostName.set(j - 1, tmp_str);
                }
            }
        }

        //AnalysisRank_Data = new String[cntRank.size()][2];

        for(i = 0; i < cntRank.size(); i++) {
            //System.out.println(HostRank.get(i) + "  " + cntRank.get(i));
            InDa.AR_DataName().add(HostRank.get(i));
            InDa.AR_Datacnt().add(String.valueOf(cntRank.get(i)));
        }

    }

    /*public String[][] get_AnalysisTime_Data() {
        return AnalysisTime_Data;
    }*/

    /*public String[][] get_AnalysisHost_Data() {
        return AnalysisHost_Data;
    }*/

    /*public String[][] getAnalysisRank_Data() {
        return AnalysisRank_Data;
    }*/

}
