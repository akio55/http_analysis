import com.sun.org.apache.xpath.internal.operations.Bool;
import sun.security.krb5.internal.PAData;

import java.io.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by akio on 2017/05/07.
 */
public class FileInput_Model {

    //private Input_Data InDa;
    private Input_Data InDa;
    private File[] FileName;
    private Boolean LenFlag;
    private String LenStart;
    private String LenEnd;

    public void Input(Input_Data tmp_InDa, File[] tmp_file, Boolean tmp_Flag, String tmp_Start, String tmp_End) {
        int i;

        InDa = tmp_InDa;
        FileName = tmp_file;
        LenFlag = tmp_Flag;
        LenStart = tmp_Start;
        LenEnd = tmp_End;

        for(i = 0; i < FileName.length; i++) {
            FileIO(FileName[i].getPath());
        }
    }

    private void FileIO(String filepath) {
        try {
            FileReader fr = new FileReader(filepath);
            BufferedReader br = new BufferedReader(fr);

            String line;

            while ((line = br.readLine()) != null) {
                System.out.println(line);
                Text_Split(line);
            }

            br.close();
            fr.close();
        } catch (FileNotFoundException e) {
            System.out.println(e.getMessage());
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }

    }

    public void Text_Split(String str) {
        int i;
        String[] tmp;
        String tmp2;

        //Pattern p = Pattern.compile("(.+)\\s(.+)\\s(.+)\\s(\\[.+\\])\\s(\".+\")\\s(.+)\\s(.+)\\s(\".+\")\\s(\".+\")");
        Pattern p = Pattern.compile("(.+)\\s(.+)\\s(.+)\\s(\\[.+\\])");
        Matcher m = p.matcher(str);

        m.find();


        //System.out.println(InDa.get_HostName(0));

        Pattern p2 = Pattern.compile("\\[(.+)\\/(.+)\\/(.+)\\:(.+)\\:(.+)\\:(.+)\\s(.+)\\]");
        Matcher m2 = p2.matcher(m.group(4));

        m2.find();

        String tmp_month = Change_Month(m2.group(2));
        String tmp_Data = m2.group(3) + tmp_month + m2.group(1);

        System.out.println(tmp_Data);

        if(LenFlag) {
            int tmp_LenStart = Integer.parseInt(LenStart);
            int tmp_LenEnd = Integer.parseInt(LenEnd);
            int tmp_Data2 = Integer.parseInt(tmp_Data);


            if(tmp_LenStart >= tmp_Data2 || tmp_Data2 <= tmp_LenEnd) {
                Data_set(m, m2);
            }
        } else {
            Data_set(m, m2);
        }

        //System.out.println(InDa.get_HostName(0) + " " + InDa.get_Time_Year(0) + "/" + InDa.get_Time_Month(0) + "/" + InDa.get_Time_Day(0) +
        //            " " + InDa.get_Time_Hour(0) + ":" + InDa.get_Time_Minute(0));

    }

    private void Data_set(Matcher tmp_m, Matcher tmp_m2) {
        InDa.HostName().add(tmp_m.group(1));
        InDa.TimeDay().add(Integer.parseInt(tmp_m2.group(1)));
        InDa.TimeMonth().add(tmp_m2.group(2));
        InDa.TimeYear().add(Integer.parseInt(tmp_m2.group(3)));
        InDa.TimeHour().add(Integer.parseInt(tmp_m2.group(4)));
        //InDa.set_Time_Minute(Integer.parseInt(tmp_m2.group(5)));
    }

    private String Change_Month(String str) {
        switch (str) {
            case "Jan":
                return "01";
            case "Feb":
                return "02";
            case "Mar":
                return "03";
            case "Apr":
                return "04";
            case "May":
                return "05";
            case "Jun":
                return "06";
            case "Jul":
                return "07";
            case "Aug":
                return "08";
            case "Sep":
                return "09";
            case "Oct":
                return "10";
            case "Nov":
                return "11";
            case "Dec":
                return "12";
            default:
                return "0";
        }
    }

}
