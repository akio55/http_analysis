import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.lang.annotation.Target;

/**
 * Created by akio on 2017/05/08.
 */
public class View {

    private JButton FileSelect;
    private DefaultTableModel TableModel_Time;
    private DefaultTableModel TableModel_Host;
    private DefaultTableModel TableModel_Rank;
    private JTextField LenStart;
    private JTextField LenEnd;
    private JButton LenSet;

    public JPanel panel() {
        JPanel panel = new JPanel();

        panel.setLayout(null);

        FileSelect = new JButton();
        FileSelect.setBounds(50,10,150,25);

        JLabel label1 = new JLabel("各時間毎のアクセス件数");
        label1.setBounds(50,45,150, 25);

        String[] Columns1 = {"時間", "回数"};
        TableModel_Time = new DefaultTableModel(Columns1, 0);
        JTable TableTime = new JTable(TableModel_Time);
        //table_time.setBounds(50,70,400,300);
        JScrollPane sp1 = new JScrollPane(TableTime);
        sp1.setBounds(50,70,400,150);

        JLabel label2 = new JLabel("リモートホスト別アクセス数");
        label2.setBounds(50,230,200,25);

        String[] Columns2 = {"ホスト名", "アクセス数"};
        TableModel_Host = new DefaultTableModel(Columns2, 0);
        JTable TableHost = new JTable(TableModel_Host);
        JScrollPane sp2 = new JScrollPane(TableHost);
        sp2.setBounds(50, 265,400,150);

        JLabel label3 = new JLabel("アクセスの多いリモートホストの順");
        label3.setBounds(50,425,250,25);

        TableModel_Rank = new DefaultTableModel(Columns2, 0);
        JTable TableRank = new JTable(TableModel_Rank);
        JScrollPane sp3 = new JScrollPane(TableRank);
        sp3.setBounds(50, 460,400,150);

        JLabel label4 = new JLabel("期間指定");
        label4.setBounds(500,45,150,25);
        JLabel label5 = new JLabel("始まり");
        label5.setBounds(500,80,150,25);
        LenStart = new JTextField();
        LenStart.setBounds(670,80,150,25);

        JLabel label6 = new JLabel("終わり");
        label6.setBounds(500,115,150,25);
        LenEnd = new JTextField();
        LenEnd.setBounds(670,115,150,25);

        LenSet = new JButton("teste");
        LenSet.setBounds(500,150,150,25);

        panel.add(FileSelect);
        panel.add(label1);
        panel.add(sp1);
        panel.add(label2);
        panel.add(sp2);
        panel.add(label3);
        panel.add(sp3);
        panel.add(label4);
        panel.add(label5);
        panel.add(LenStart);
        panel.add(label6);
        panel.add(LenEnd);
        panel.add(LenSet);
        return panel;
    }

    public JButton get_FileSelect() {
        return FileSelect;
    }

    public DefaultTableModel get_TableModel_Time() {
        return TableModel_Time;
    }

    public DefaultTableModel get_TableModel_Host() {
        return TableModel_Host;
    }

    public DefaultTableModel get_TableModel_Rank() {
        return TableModel_Rank;
    }

    public JTextField get_LenStart() {
        return LenStart;
    }

    public JTextField get_LenEnd() {
        return LenEnd;
    }

    public JButton get_LenSet() {
        return LenSet;
    }
}
